package configutils

import (
	"gitlab.com/patrickmckowen/filepathutils"
	"fmt"
	"github.com/spf13/viper"
	"log"
	"path/filepath"
)

// Unmarshal into struct. Struct fields should match keys from config (case in-sensitive)
type Config struct {
	Host    string
	Port    int
	enabled bool
}

func InitConfig(config_path string) Config {
	viper.AddConfigPath(filepath.Join(filepathutils.Get_home_dir(), config_path))
	viper.SetConfigName("config")

	// Searches for config file in given paths and read it
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	// Confirm which config file is used
	fmt.Printf("Using config: %s\n", viper.ConfigFileUsed())

	// Picking config level
	prod := viper.Sub("prod")

	//
	var C Config

	//
	err := prod.Unmarshal(&C)

	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	return C
}
